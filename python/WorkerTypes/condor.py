import subprocess
import time
import os

from Core.worker import Worker

submitFile = """universe = vanilla
executable = $(DIR)/work.sh
getenv = True
log = $(DIR)/log.txt
error = $(DIR)/err.txt
output = $(DIR)/out.txt
+MaxRuntime = {}
"""

class CondorWorker(Worker):
    currentWaiting = {}
    waitedTime = 0
    maxrunTime = 20 * 60

    def __init__(self, workConfig = {}):
        super().__init__(workConfig)
        
        self.currentLogLine = 0

    def doWork(self):    
        ##Add worker to waiting line of condor jobs
        CondorWorker.currentWaiting[self["name"]] = self
        CondorWorker.waitedTime = 0

    def getWorkStatus(self):
        if CondorWorker.waitedTime >= 20000 or len(CondorWorker.currentWaiting) > 200:
            ##Write tmp sumbit file
            with open("/tmp/jobs.sub".format(self["dir"]), "w") as subFile:
                subFile.write(submitFile.format(CondorWorker.maxrunTime))
                        
            ##Call submit
            result = subprocess.run("condor_submit /tmp/jobs.sub -queue DIR in " +  " ".join([w["dir"] for w in CondorWorker.currentWaiting.values()]), shell = True, capture_output=True)
            
            if result.returncode != 0:
                raise RuntimeError("Condor jobs can not be submitted, because:\n{}".format(result.stderr.decode("utf-8")))
                       
            ##Clear waiting line
            CondorWorker.currentWaiting.clear()
            CondorWorker.waitedTime = 0
            
        elif len(CondorWorker.currentWaiting) != 0:
            CondorWorker.waitedTime += 1
            
        ##If log does not exist, job was not submitted yet
        if not os.path.exists("{}/log.txt".format(self["dir"])):
            return self["status"]
       
        ##Check status based on read out log.txt  
        with open("{}/log.txt".format(self["dir"]), "r") as logFile:
            condorLog = logFile.readlines()[self.currentLogLine:]
            self.currentLogLine += len(condorLog) - 1 
            condorLog = "".join(condorLog)
                         
        status = self["status"]
                      
        if "Job submitted" in condorLog:
            if "Job executing" in condorLog:
                status = "Working"
                
            else:
                status = "Submitted"
                         
        if "Normal termination" in condorLog:
            if "(return value 0)" in condorLog:
                status = "Finished"
                           
            else:
                status = "Failed"

        if "SYSTEM_PERIODIC_REMOVE" in condorLog:
            status = "Failed"
                
        return status
