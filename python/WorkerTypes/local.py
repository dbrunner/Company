from concurrent.futures import ProcessPoolExecutor
import subprocess
import time
import os

from Core.worker import Worker

class LocalWorker(Worker):
    pool = ProcessPoolExecutor(os.cpu_count())

    def __init__(self, workConfig = {}):
        super().__init__(workConfig)

    def doWork(self):    
        ##Submit job to pool
        self.workStatus = LocalWorker.pool.submit(subprocess.run, 
                                                 ["nice", "{}/work.sh".format(self["dir"])],
                                                 capture_output=True)

        with open("{}/log.txt".format(self["dir"]), "a") as log:
            log.write("[{}] Worker started to wait in the local queue.\n\n".format(time.asctime()))

    def getWorkStatus(self):
        if self.workStatus.running():
            return "Working"

        elif self.workStatus.done():
            with open("{}/log.txt".format(self["dir"]), "a") as log, \
                 open("{}/out.txt".format(self["dir"]), "a") as out, \
                 open("{}/err.txt".format(self["dir"]), "a") as err:
                 
                if self.workStatus.result().returncode == 0:
                    log.write("[{}] Company asked for status with returnvalue '{}'.\n\n".format(time.asctime(), "Finished"))
                    out.write(self.workStatus.result().stdout.decode("utf-8"))
                    
                    return "Finished"
                    
                else:
                    log.write("[{}] Company asked for status with returnvalue '{}'.\n\n".format(time.asctime(), "Failed"))
                    out.write(self.workStatus.result().stdout.decode("utf-8"))
                    err.write(self.workStatus.result().stderr.decode("utf-8"))
                    
                    return "Failed"
              
        else:
            return self["status"]
