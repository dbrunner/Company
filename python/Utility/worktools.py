def beautifyCommand(command):
    beautifiedCommand = ""

    if type(command) == str:
        splittedCommand = command.split()

        beautifiedCommand += splittedCommand[0] + " \\\n"

        for line in splittedCommand[1:]:
            beautifiedCommand += 4*" " + line + " \\\n"

        beautifiedCommand += "\n"

    elif type(command) == dict:
        beautifiedCommand += command["exe"] + " \\\n"
        
        for arg in command["arguments"]:
            beautifiedCommand += 4*" " + line + " \\\n"
            
    return beautifiedCommand
