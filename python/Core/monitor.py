import curses
import time
from datetime import datetime
import os
import math
import json

from collections import deque 

class Monitor:
    def __init__(self, dirName):
        os.putenv("TERM", "xterm-256color") 
        self.isActive = True
        
        self.screen = curses.initscr()
        self.rows, self.cols = self.screen.getmaxyx()
        
        curses.start_color()
        curses.use_default_colors()
        
        self.colorMap = {}
        
        with open("{}/config/colors.json".format(os.environ["COMPANYDIR"]) ,"r") as colorFile:
            colors = json.load(colorFile)
            colorNumber = 200
        
            for status in colors.keys():
                R, B, G = colors[status]["RBG"]
            
                curses.init_color(colorNumber, int(R/0.255), int(B/0.255), int(G/0.255))
                curses.init_pair(colorNumber, colorNumber, -1)
                self.colorMap[status] = colorNumber
        
                colorNumber += 1
        
        curses.noecho()
        curses.cbreak()
        curses.curs_set(0)
        self.screen.keypad(True)
  
        self.overviewLength = 6
        self.logFile = open("{}/companyLog.txt".format(dirName), "w")
        self.maxLogLen = self.rows - self.overviewLength - 4
        self.log = deque([])
        
        self.initialise()
        self.start = datetime.now()
        
    def errorHandler(func):
        def decoratedFunc(self, *args):
            try:
                func(self, *args)

            except Exception as e:
                pass
        
        return decoratedFunc

    def calcX(self, string):
        return math.floor(self.cols/2.) - math.floor(len(string)/2.)
        
    def calcSeveralX(self, messageList, groupSize = 1):
        totalLen = sum([len(m) for m in messageList])
        gap = math.floor((self.cols - 4 - totalLen)/(len(messageList)/groupSize + 1))
        
        xPos = [2 + gap]
        
        for i, m in enumerate(messageList):
            if i % groupSize != 0:
                xPos.append(xPos[-1] + len(m) + gap)
                
            else:
                xPos.append(xPos[-1] + len(m))
      
        return xPos

    def initialise(self):
        self.rows, self.cols = self.screen.getmaxyx()
        self.maxLogLen = self.rows - self.overviewLength - 4
    
        title = "OVERVIEW"
        self.overview = curses.newwin(self.overviewLength, self.cols, 0, 0)
        self.overview.border('.', '.', '.', '.', '.', '.', '.', '.')
        self.overview.addstr(1, self.calcX(title), title, curses.A_BOLD)
        self.overview.addstr(2, 1, "."*(self.cols-2))
        self.overview.refresh()
        
        title = "LOGGING"
        self.logWindow = curses.newwin(self.rows - self.overviewLength, self.cols, self.overviewLength, 0)
        self.logWindow.border('.', '.', '.', '.', '.', '.', '.', '.')
        self.logWindow.addstr(1, self.calcX(title), title, curses.A_BOLD)
        self.logWindow.addstr(2, 1, "."*(self.cols-2))
        self.logWindow.refresh()

    @errorHandler
    def updateOverview(self, workStatus):
        if self.overviewLength < 6 + (len(workStatus) - 1 ) * 5:
            self.overviewLength = 6 + (len(workStatus) - 1 ) * 5
            self.initialise()
        
        if (self.rows, self.cols) != self.screen.getmaxyx():
            self.initialise()
        
        self.overview.addstr(3, self.calcX("General"), "General", curses.A_BOLD | curses.A_UNDERLINE)   
             
        message = [
            ("Time", None),
            (": {:.2f} s".format((datetime.now() - self.start).total_seconds()), None)
        ]
        
        xPos = self.calcSeveralX([m[0] for m in message], 2)
        
        for x, (m, style) in zip(xPos, message):
            if style == None:
                self.overview.addstr(5, x, m)
                
            else:
                self.overview.addstr(5, x, m, style)
        
        nRows = 7
        
        for i, workerType in enumerate(workStatus):
            if workerType == "nFinished":
                continue
        
            self.overview.addstr(nRows, self.calcX(workerType), workerType, curses.A_BOLD | curses.A_UNDERLINE)        
            self.overview.addstr(nRows + 1, 1, " "*(self.cols-2))

            message = []

            for status in workStatus[workerType].keys():
                message.append((status, curses.color_pair(self.colorMap[status]) | curses.A_BOLD))
                message.append((": {}".format(workStatus[workerType][status]), None))
            
            xPos = self.calcSeveralX([m[0] for m in message], 2)
            
            self.overview.addstr(nRows + 2, 1, " "*(self.cols-2))
            
            for x, (m, style) in zip(xPos, message):            
                if style == None:
                    self.overview.addstr(nRows + 2, x, m)
                
                else:
                    self.overview.addstr(nRows + 2, x, m, style)
                    
            nRows += 4
                
        self.overview.refresh()
        
    def addToLog(self, name, oldStatus, newStatus):
        message = [
            ("[{}]: '".format(" ".join(time.ctime().split()[1:-1])), None),
            (name, None),
            ("' changed from ", None),
            (oldStatus, curses.A_BOLD | curses.color_pair(self.colorMap.get(oldStatus, 7))),
            (" to ", None),
            (newStatus, curses.A_BOLD | curses.color_pair(self.colorMap.get(newStatus, 7)))
        ]      
                
        self.log.append(message)
 
    @errorHandler
    def updateLog(self):
        while len(self.log) > self.maxLogLen:
            oldest = self.log.popleft()
            self.logFile.write("".join([m[0] for m in oldest]) + "\n")  
              
        for l in range(self.maxLogLen):
            self.logWindow.addstr(l+3, 1, " "*(self.cols-2))
              
        for l, message in enumerate(self.log):
            currentLen = 1
            messLen = len("".join([m[0] for m in message]))
            
            for pIdx, part in enumerate(message):
                mes, style = part
                
                if messLen > self.cols-2 and pIdx == 1:
                    mes = mes[:-(messLen - (self.cols - 2) + 3)] + "..."
            
                if style == None:
                    self.logWindow.addstr(l+3, currentLen, mes)
                
                else:
                    self.logWindow.addstr(l+3, currentLen, mes, style)
                    
                currentLen += len(mes)
            
        self.logWindow.refresh()

    def close(self):
        while self.log:
            oldest = self.log.popleft()
            self.logFile.write("".join([m[0] for m in oldest]) + "\n")
    
        content = []
        
        for i in range(self.overviewLength):
            content.append(self.overview.instr(i, 0))
            
        curses.nocbreak()
        self.screen.keypad(False)
        curses.echo()
        curses.endwin()
        
        return content
