import threading
import json
import os
import time
import shutil
import http.server

from collections import deque
from functools import partial

from Core.monitor import Monitor

class Company:
    def __init__(self, dirName = "MyCompany", port = 2000, dryRun = False, existingCompany = ""):
        self.dir = os.path.abspath(dirName)
        os.makedirs(self.dir, exist_ok = True)

        ## Related to threading exception handling
        threading.excepthook = self.businessInsurance
        self.currentExc = None
        self.lock = threading.Lock() 

        ## Container for workers
        self.workers = {}
        self.workQueue = deque()
        self.workStatus = {"nFinished": 0}
        self.dryRun = dryRun

        ## Company logging and graphing
        self.companyRecord = {}
        self.companyGraph = {"root": []}

        if not dryRun:
            ##Load existing company if given
            if existingCompany != "":   
                with open(existingCompany, "r") as oldCompany:
                    self.companyRecord = json.load(oldCompany)
                    
            else:
                self.dump()
            
            ## Start http hosting in other thread aswell
            try:
                http_handler = http.server.SimpleHTTPRequestHandler
                http_handler.log_message = lambda a, b, c, d, e: None
                self.companyPage = http.server.HTTPServer(("localhost", port), partial(http_handler, directory=self.dir))
                self.webHoster = threading.Thread(target=self.hostServer, args=())
                self.webHoster.start()
              
            except:
                self.companyPage = None
              
            ##Start main loop in other thread
            self.close = False
            self.business = threading.Thread(target=self.doBusiness, args=())
            self.business.start()
                                    
            ## ncurses monitor class
            self.monitor = Monitor(dirName)
        
    def __enter__(self):
        return self    
        
    def __exit__(self, exc_type, exc_value, exc_traceback):
        if self.dryRun:
            self.dump()
        
            return
    
        self.close = True
        
        ## Wait for doBusiness function and catch KeyboardInterrupt
        try:
            self.business.join()
                
        except (KeyboardInterrupt, Exception) as e:
            if self.currentExc == None:
                self.currentExc = e
                    
            self.business.join()
               
        ## Clean up
        content = self.monitor.close()
        
        if self.companyPage != None: 
            self.companyPage.shutdown()
            self.webHoster.join()
        
        ## If exception occured, finally reraise it here
        if self.currentExc != None: 
            print("Your company is broke and your shares are worthless. Check the traceback below for more details.\n")
            raise self.currentExc
            
        elif exc_value:
            print("Your company is broke and your shares are worthless. Check the traceback below for more details.\n")
            raise exc_value.with_traceback(exc_traceback)
            
        ## Else just print finishing screen
        print("Business is booming! Your company is sucessful and will close for today. See the closing statistics below.")
        for line in content:
            print(line.decode("utf-8"))

    def businessInsurance(self, exc):
        if self.currentExc == None:
            self.currentExc = exc.exc_value.with_traceback(exc.exc_traceback)

    def hostServer(self):   
        if threading.main_thread() == threading.current_thread():
            raise RuntimeError("Please dont call 'hostServer' function on your own, it is called automatically on creation!") 

        shutil.copyfile("{}/html/company.html".format(os.environ["COMPANYDIR"]), "{}/company.html".format(self.dir))
        self.companyPage.serve_forever()
           
    def dump(self):
        with open("{}/companyRecord.json".format(self.dir), "w") as f:
            json.dump(self.companyRecord, f, ensure_ascii=False, indent=4)

        with open("{}/companyGraph.json".format(self.dir), "w") as f:
            json.dump(self.companyGraph, f, ensure_ascii=False, indent=4)
 
    def employWorker(self, worker):
        with self.lock:
            ## If exception is catched somewhere, stop execution by reraise empty
            if self.currentExc != None:
                raise
                
            ## Check if worker name duplicate
            if worker["name"] in self.workers:
                raise RuntimeError("Atleast two workers has the same name: '{}'".format(worker["name"]))
                
            ## Check if worker share same dir
            worker["hashed-dir"] = hash(worker["dir"])
            
            for wName, w in self.workers.items():
                if worker["hashed-dir"] == w["hashed-dir"]:
                    raise RuntimeError("Atleast two workers '{}, {}' has the same directory: '{}'".format(w["name"], worker["name"], worker["dir"]))
               
            ## Add worker to dict of workers
            self.workers[worker["name"]] = worker
            
            if worker["name"] not in self.companyRecord:
                self.companyRecord[worker["name"]] = {"status": worker["status"], "dir": worker["dir"], "type": worker.alias}
            
            ## Dry run, only setup dir and return            
            if self.dryRun:
                print("[Dry run] Prepared workplace for worker '{}'".format(worker["name"]))
                worker.prepareWork()
                
                return

            ## If no dependencies, put right away in the queue
            if self.companyRecord[worker["name"]]["status"] == "Finished":
                worker.updateStatus(self.monitor, self.companyRecord, self.workStatus, "Finished")
                self.workStatus["nFinished"] += 1
            
            elif len(worker["dependencies"]) == 0:            
                worker.prepareWork()
                worker.doWork()
                worker.updateStatus(self.monitor, self.companyRecord, self.workStatus, "Queued")
                    
                self.workQueue.append(worker)
                self.companyGraph.setdefault("root", []).append(worker["name"])
                self.companyGraph.setdefault(worker["name"], [])

            ## If there are dependencies, check number of  workers to wait for
            else:            
                worker.prepareWork()
                worker.nWaiting = len(worker["dependencies"])
                worker.updateStatus(self.monitor, self.companyRecord, self.workStatus, "Queued")
               
                self.companyGraph.setdefault(worker["name"], [])
                 
                ## Loop over dependencies   
                for dep in worker["dependencies"]:
                    self.companyGraph.setdefault(dep, []).append(worker["name"])
                        
                    ## If already finished worker, reduce number of worker to wait for
                    if dep in self.workers and self.workers[dep]["status"] == "Finished":
                        worker.nWaiting -= 1
         
                ## If all workers already finished, put job in queue
                if worker.nWaiting == 0:
                    worker.doWork()
                    self.workQueue.append(worker)

    def getWorker(self, workerName):
        with self.lock:
            return self.workers.get(workerName, None)

    def doBusiness(self):
        if threading.main_thread() == threading.current_thread():
            raise RuntimeError("Please dont call 'doBusiness' function on your own, it is called automatically on creation!")
            
        while True:
            ## If exception is catched somewhere, stop execution by reraise empty
            if self.currentExc != None:
                raise

            ##All worker are done and no new employer will join
            if self.workStatus["nFinished"] == len(self.workers) and self.close:
                return

            ##Queue is empty, but new employer can be hired   
            if len(self.workQueue) == 0:
                if self.close:
                    raise RuntimeError("No workers in the queue, but there are workers waiting for other workers. Check for cyclic dependencies!")
               
                continue
                             
            ##Check latest worker status            
            currentWorker = self.workQueue.popleft()
            
            with self.lock:
                workStatus = currentWorker.getWorkStatus()
           
            currentWorker.updateStatus(self.monitor, self.companyRecord, self.workStatus, workStatus)
           
            if time.time() - os.path.getmtime("{}/companyRecord.json".format(self.dir)) > 10:
                with self.lock:
                    self.dump()
                       
            if workStatus == "Finished":
                ##If finished, check next workers in line and send them to work
                self.workStatus["nFinished"] += 1
                   
                for next in self.companyGraph[currentWorker["name"]]:
                    self.workers[next].nWaiting -= 1

                    if self.workers[next].nWaiting == 0:
                        self.workers[next].doWork()
                        self.workQueue.append(self.workers[next])        

            elif workStatus == "Failed":
                if currentWorker.nFailed >= currentWorker["max-retries"]:
                    raise RuntimeError("Worker '{}' failed his job {} times. Check out the error log '{}/err.txt'".format(currentWorker["name"], currentWorker["max-retries"], currentWorker["dir"]))
               
                else:
                    currentWorker.nFailed += 1
                    
                    with self.lock:
                        currentWorker.updateStatus(self.monitor, self.companyRecord, self.workStatus, "Queued")

                    currentWorker.doWork()
                    self.workQueue.append(currentWorker)
               
            else:
                ##No end of working day yet for this worker
                self.workQueue.append(currentWorker)
