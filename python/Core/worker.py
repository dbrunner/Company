import os
import json
from abc import ABC, abstractmethod

from Utility.worktools import beautifyCommand

class Worker(dict, ABC):
    def __init__(self, workConfig = {}):
        self["name"] = "Worker_{}".format(id(self))
        self["dir"] = "Workplace_{}".format(id(self))
        self["status"] = "None"
        self["max-retries"] = 3
        self["dependencies"] = []
        self["list-of-commands"] = []

        self.update(workConfig)
        self["dir"] = os.path.abspath(self["dir"])

        self.nFailed = 0
        self.alias = type(self).__name__

    def prepareWork(self):
        ##Create working directory
        os.makedirs(self["dir"], exist_ok = True)

        ##Write down self in JSON format
        with open("{}/worker.json".format(self["dir"]), "w") as workerFile:
            json.dump(dict(self), workerFile, ensure_ascii=False, indent=4)

        ##Clean up potential previous work logs
        for logType in ["log", "out", "err"]:
            fileName = "{}/{}.txt".format(self["dir"], logType)

            if os.path.exists(fileName):
                os.remove(fileName)

        ##Create runable shell executable
        with open("{}/work.sh".format(self["dir"]), "w") as workFile:
            workFile.write("#!/bin/bash\n")

            for command in self["list-of-commands"]:
                workFile.write(beautifyCommand(command))

            os.chmod("{}/work.sh".format(self["dir"]), 0o755)

    def updateStatus(self, monitor, graph, workStatus, newStatus):    
        monitor.updateOverview(workStatus)
        monitor.updateLog()
    
        if(self["status"] == newStatus):
            return
        
        if self.alias not in workStatus:
            workStatus[self.alias] = {}
            
        if self["status"] != "None" and self["status"] not in workStatus[self.alias]:
            workStatus[self.alias][self["status"]] = 0
            
        if newStatus not in workStatus[self.alias]:
            workStatus[self.alias][newStatus] = 0
        
        if self["status"] != "None":
            workStatus[self.alias][self["status"]] -= 1
            
        workStatus[self.alias][newStatus] += 1
        
        monitor.updateOverview(workStatus)
        monitor.addToLog(self["name"], self["status"], newStatus)
        monitor.updateLog()
            
        graph[self["name"]]["status"] = newStatus
        self["status"] = newStatus

    @abstractmethod
    def doWork(self):
        pass

    @abstractmethod
    def getWorkStatus(self):
        pass
