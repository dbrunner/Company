from WorkerTypes.condor import CondorWorker
from WorkerTypes.local import LocalWorker
from Core.company import Company

d = "Test2"

with Company(dirName = d) as c:
    for i in range(3):
        w = LocalWorker({"name": "Worker_{}".format(i), "list-of-commands": ["sleep 10"], "dir": "{}/Workplace{}".format(d, i)})
        c.employWorker(w)

        for j in range(3):
            w = LocalWorker({"name": "Worker_{}_{}".format(i, j), 
                             "list-of-commands": ["echo hello"], 
                             "dir": "{}/Workplace_{}_{}".format(d, i, j),
                             "dependencies": ["Worker_{}".format(n) for n in range(2)]})

            c.employWorker(w)

            for k in range(3):
                w = LocalWorker({"name": "Worker_{}_{}_{}".format(i, j, k), 
                                 "list-of-commands": ["echo hello"], 
                                 "dir": "{}/Workplace_{}_{}_{}".format(d, i, j, k), 
                                 "dependencies": ["Worker_{}_{}".format(n, b) for n in range(2) for b in range(2)]})
            
                c.employWorker(w)

                for o in range(3):
                    w = LocalWorker({"name": "Worker_{}_{}_{}_{}".format(i, j, k, o), 
                                     "list-of-commands": ["echo hello"], 
                                     "dir": "{}/Workplace_{}_{}_{}_{}".format(d, i, j, k, o), 
                                     "dependencies": ["Worker_{}_{}_{}".format(n, x, y) for n in range(2) for x in range(2) for y in range(2)]})
                
                    c.employWorker(w)
